

# 实验6（期末考核） 基于Oracle数据库的商品销售系统的设计 

学号：202010414322    姓名：辛尚豪

## 期末考核要求

- 实验在自己的计算机上完成。
- 文档`必须提交`到你的oracle项目中的test6目录中。test6目录中必须至少有3个文件：
  - test6.md主文件。
  - 数据库创建和维护用的脚本文件*.sql。
  - [test6_design.docx](./test6_design.docx)，学校格式的完整报告。
- 文档中所有设计和数据都必须是独立完成的真实实验结果。不得抄袭，杜撰。
- 提交时间： 2023-5-26日前

## 评分标准

| 评分项|评分标准|满分|
|:-----|:-----|:-----|
|文档整体|文档内容详实、规范，美观大方|10|
|表设计|表设计及表空间设计合理，样例数据合理|20|
|用户管理|权限及用户分配方案设计正确|20|
|PL/SQL设计|存储过程和函数设计正确|30|
|备份方案|备份方案设计正确|20|









# 具体设计过程

## 一.表及表空间设计方案

 表空间设计：

 表空间1：用于存储系统表、索引和常用的表。

 表空间2：用于存储大型表和历史数据。 

代码如下

```sql
-- 创建表空间
CREATE TABLESPACE space1 DATAFILE '/home/oracle/oradata/space1.dbf' SIZE 500M;
CREATE TABLESPACE space2 DATAFILE '/home/oracle/oradata/space2.dbf' SIZE 500M; 

```

![image-20230526093819352](image-20230526093819352.png) 

```sql
    -- 创建商品表
 CREATE TABLE products (
    product_id NUMBER PRIMARY KEY,
    product_name VARCHAR2(100) NOT NULL,
    category VARCHAR2(50),
    price NUMBER(10, 2) NOT NULL,
    stock_quantity NUMBER NOT NULL,
    description VARCHAR2(500)
);


    -- 创建客户表
   CREATE TABLE customers (
    customer_id NUMBER PRIMARY KEY,
    customer_name VARCHAR2(50) NOT NULL,
    phone VARCHAR2(20)UNIQUE NOT NULL,
    address VARCHAR2(200)
);


    -- 创建订单表
CREATE TABLE orders (
    order_id NUMBER PRIMARY KEY,
    customer_id NUMBER,
    order_date DATE NOT NULL,
    total_amount NUMBER(10, 2) NOT NULL,
    FOREIGN KEY (customer_id) REFERENCES customers(customer_id)
);


    -- 创建订单详情表
CREATE TABLE order_details (
    order_detail_id NUMBER PRIMARY KEY,
    order_id NUMBER,
    product_id NUMBER,
    quantity NUMBER NOT NULL,
    price NUMBER(10, 2) NOT NULL,
    FOREIGN KEY (order_id) REFERENCES orders(order_id),
    FOREIGN KEY (product_id) REFERENCES products(product_id)
);

```

![image-20230526093907321](image-20230526093907321.png) 

![image-20230526094035579](image-20230526094035579.png) 

![image-20230526094110350](image-20230526094110350.png) 

![image-20230526094205518](image-20230526094205518.png) 

![image-20230526094453699](image-20230526094453699.png) 

 



## 二.权限及用户分配方案

```sql
-- 管理员用户
CREATE USER pdbadmin IDENTIFIED BY 123;
GRANT CONNECT, RESOURCE, DBA，CREATE SESSION TO pdbadmin;

-- 普通用户
CREATE USER pdbuser IDENTIFIED BY 123;
GRANT CONNECT, RESOURCE TO user;
GRANT SELECT, INSERT, UPDATE, DELETE ON products TO pdbuser;
GRANT SELECT, INSERT, UPDATE, DELETE ON customers TO pdbuser;
GRANT SELECT, INSERT, UPDATE, DELETE ON orders TO pdbuser;
GRANT SELECT, INSERT, UPDATE, DELETE ON order_details TO pdbuser;
```

![image-20230526094854695](image-20230526094854695.png) 

##  三.创建程序包并添加存储过程和函数 

```sql
CREATE OR REPLACE PACKAGE my_package AS
  /* 存储过程：根据订单ID查询订单详情 */
  PROCEDURE get_order_details(
    order_id IN orders.order_id%TYPE,
    order_details_cursor OUT SYS_REFCURSOR
  );
  
  /* 存储过程：创建新订单并返回订单ID */
  PROCEDURE create_order(
    customer_id IN customers.customer_id%TYPE,
    order_date IN orders.order_date%TYPE,
    total_amount IN orders.total_amount%TYPE,
    order_id OUT orders.order_id%TYPE
  );
  
  /* 存储过程：添加商品到订单中 */
  PROCEDURE add_product_to_order(
    order_id IN order_details.order_id%TYPE,
    product_id IN order_details.product_id%TYPE,
    quantity IN order_details.quantity%TYPE,
    price IN order_details.price%TYPE
  );
  
  /* 函数：根据商品ID获取商品信息 */
  FUNCTION get_product_info(
    product_id IN products.product_id%TYPE
  ) RETURN VARCHAR2;
END my_package;
/

CREATE OR REPLACE PACKAGE BODY my_package AS
  /* 存储过程：根据订单ID查询订单详情 */
  PROCEDURE get_order_details(
    order_id IN orders.order_id%TYPE,
    order_details_cursor OUT SYS_REFCURSOR
  ) IS
  BEGIN
    OPEN order_details_cursor FOR
      SELECT * FROM order_details WHERE order_id = order_id;
  END;
  
  /* 存储过程：创建新订单并返回订单ID */
  PROCEDURE create_order(
    customer_id IN customers.customer_id%TYPE,
    order_date IN orders.order_date%TYPE,
    total_amount IN orders.total_amount%TYPE,
    order_id OUT orders.order_id%TYPE
  ) IS
  BEGIN
    SELECT orders_seq.NEXTVAL INTO order_id FROM DUAL;
    
    INSERT INTO orders(order_id, customer_id, order_date, total_amount)
    VALUES(order_id, customer_id, order_date, total_amount);
  END;
  
  /* 存储过程：添加商品到订单中 */
  PROCEDURE add_product_to_order(
    order_id IN order_details.order_id%TYPE,
    product_id IN order_details.product_id%TYPE,
    quantity IN order_details.quantity%TYPE,
    price IN order_details.price%TYPE
  ) IS
  BEGIN
    INSERT INTO order_details(order_detail_id, order_id, product_id, quantity, price)
    VALUES(order_details_seq.NEXTVAL, order_id, product_id, quantity, price);
  END;
  
  /* 函数：根据商品ID获取商品信息 */
  FUNCTION get_product_info(
    product_id IN products.product_id%TYPE
  ) RETURN VARCHAR2 IS
    product_info VARCHAR2(4000);
  BEGIN
    SELECT 'Product ID: ' || products.product_id ||
           ' Product Name: ' || products.product_name ||
           ' Category: ' || products.category ||
           ' Price: ' || products.price ||
           ' Stock Quantity: ' || products.stock_quantity ||
           ' Description: ' || products.description
      INTO product_info
      FROM products
      WHERE products.product_id = product_id;
      
    RETURN product_info;
  END;
END my_package;


```

上述程序包创建了一个名为 my_package 的 PL/SQL 包，它包含了四个存储过程和一个函数。

- get_order_details 存储过程用于查询指定订单的订单详情，并通过 OUT 参数返回结果集。
- create_order 存储过程用于创建新订单，并通过 OUT 参数返回生成的订单ID。
- add_product_to_order 存储过程用于向指定订单中添加商品。
- get_product_info 函数用于查询指定商品的详细信息，并返回一个字符串。

![image-20230526102920173](image-20230526102920173.png) 

## 四.创建存储过程,每个表模拟5万条数据*

### 4-1.创建存储过程需要的序列

```sql
CREATE SEQUENCE orders_seq
  START WITH 1
  INCREMENT BY 1
  CACHE 10;
  
CREATE SEQUENCE order_details_seq
  START WITH 1
  INCREMENT BY 1
  CACHE 10;
```

### 4-2.创建存储过程

```sql
CREATE OR REPLACE PROCEDURE insert_data AS
  -- 插入商品表数据
  CURSOR products_cur IS
    SELECT LEVEL product_id, 'Product '||LEVEL product_name,
           CASE MOD(LEVEL, 5)
             WHEN 1 THEN 'Books'
             WHEN 2 THEN 'Electronics'
             WHEN 3 THEN 'Clothing'
             WHEN 4 THEN 'Home'
             ELSE 'Sports'
           END category,
           ROUND(DBMS_RANDOM.VALUE(10, 1000), 2) price,
           ROUND(DBMS_RANDOM.VALUE(50, 500), 0) stock_quantity,
           'Description for product '||LEVEL description
    FROM DUAL
    CONNECT BY LEVEL <= 50000;
    
  -- 插入客户表数据
  CURSOR customers_cur IS
    SELECT LEVEL customer_id, 'Customer '||LEVEL customer_name,
           '1234567890' || LPAD(LEVEL, 2, '0') phone,
           'Address for customer '||LEVEL address
    FROM DUAL
    CONNECT BY LEVEL <= 50000;
  
  -- 插入订单表和订单详情表数据
  v_order_id orders.order_id%TYPE;
  v_product_id order_details.product_id%TYPE;
BEGIN
  -- 批量插入商品表
  FOR product IN products_cur LOOP
    INSERT INTO products(product_id, product_name, category, price, stock_quantity, description)
    VALUES(product.product_id, product.product_name, product.category, product.price, product.stock_quantity, product.description);
  END LOOP;
  
  -- 批量插入客户表
  FOR customer IN customers_cur LOOP
    INSERT INTO customers(customer_id, customer_name, phone, address)
    VALUES(customer.customer_id, customer.customer_name, customer.phone, customer.address);
  END LOOP;
  
  -- 批量插入订单表和订单详情表
  FOR i IN 1..50000 LOOP
    -- 创建新订单并获取订单 ID
    INSERT INTO orders(order_id, customer_id, order_date, total_amount)
    VALUES(orders_seq.NEXTVAL, TRUNC(DBMS_RANDOM.VALUE(1, 50000)), SYSDATE, ROUND(DBMS_RANDOM.VALUE(100, 5000), 2))
    RETURNING order_id INTO v_order_id;
    
    -- 将 1-5 个商品添加到订单详情中
    FOR j IN 1..DBMS_RANDOM.VALUE(1, 5) LOOP
      -- 获取随机产品 ID
      SELECT product_id INTO v_product_id FROM (SELECT product_id FROM products ORDER BY DBMS_RANDOM.RANDOM) WHERE ROWNUM = 1;
      
      -- 添加订单详细信息
      INSERT INTO order_details(order_detail_id, order_id, product_id, quantity, price)
      VALUES(order_details_seq.NEXTVAL, v_order_id, v_product_id, DBMS_RANDOM.VALUE(1, 10), ROUND(DBMS_RANDOM.VALUE(10, 100), 2));
    END LOOP;
  END LOOP;
  
  COMMIT;
END insert_data;
/

```

  上面的存储过程使用了游标来分别插入商品表和客户表数据。然后创建了一个带有嵌套循环的 FOR 循环结构，插入订单表和订单详情表的数据。 由于订单表的外键约束，每个订单必须有与之关联的客户，而由于订单详情表的外键约束，每个订单详细信息条目必须有与之关联的产品和订单。因此，在生成随机数据时要考虑这些约束，以确保每个订单和订单详细信息都有正确的客户和产品 ID。

### 4-3.运行，生成数据

```sql
BEGIN
  insert_data;
END;

```

![image-20230526101638055](image-20230526101638055.png) 

![image-20230526101656018](image-20230526101656018.png) 

![image-20230526101754759](image-20230526101754759.png) 

![image-20230526101814080](image-20230526101814080.png) 



## 五.数据库备份方案

- 定义备份策略：

每日进行全量备份，备份周期为 7 天；

每小时进行增量备份，备份周期为 24 小时；

保留最近 30 天的备份数据，超过 30 天的备份数据自动清理。

 

- 选择备份工具：

RMAN (Recovery Manager)，这是 Oracle 数据库官方推荐的备份和恢复工具，具有高可靠性、高效性、可扩展性等特点，支持全量备份、增量备份、归档日志备份等多种备份模式。

 

- 配置备份环境：

备份服务器：一台专用于备份的物理服务器，至少具备 16 核 CPU、64GB 内存、100GB 存储空间等硬件配置，采用 Oracle Linux 或 Red Hat Enterprise Linux 系统。

备份存储设备：采用高可靠性的网络存储设备，如 SAN、NAS 或云存储，保证备份数据的可靠性和可恢复性。

 

- 设置备份参数：

配置全量备份和增量备份的目标路径和备份集大小；

配置自动清理策略，保留最近 30 天的备份集；

配置备份优化参数，如并行度、压缩比、校验等。

 

- 执行备份操作：

全量备份：每日凌晨 2 点进行全量备份操作，备份集保存到备份存储设备；

增量备份：每个小时的第 15 分钟进行增量备份操作，备份集保存到备份存储设备。

 

- 验证备份数据：

每周进行一次备份数据验证，检查备份集的完整性、可恢复性和性能等指标，并记录测试结果。

 

- 定期维护备份环境：

每月进行一次备份环境巡检，检查备份服务器、备份存储设备和备份工具的状态，并进行必要的维护和升级；

根据业务需求和技术变化，定期更新备份策略和备份工具参数，以适应新的业务场景和技术

## 六.总结

本课程旨在学习如何使用Oracle数据库，通过设计商品销售系统的数据结构，提高了我对数据库表空间、用户权限、存储过程和备份方案等相关概念的理解。

通过本课程，我学会了使用Oracle数据库进行数据设计，并深入了解表空间、用户权限、存储过程和备份方案等概念。此外，我也能够更加熟练地使用SQL语言进行数据库操作和管理，同时也明白了设计好的数据库可以有效地支持各种业务需求。

在使用oracle时，要注意很多问题。例如，在设计表结构时，需要分析不同表之间的关系，避免出现冗余字段或者无法满足需求的情况；在创建用户和赋予权限时，需要谨慎考虑不同用户的需求和安全性；在制定备份方案时，需要考虑不同的灾难恢复场景，以保证数据完整性和可靠性等。

总之，本课程让我更深入地了解Oracle数据库的使用和管理。
